#include "../inc/jogador.hpp"
#include "../inc/mapa.hpp"
#include <bits/stdc++.h>

Jogador::Jogador() {
    nome = "";
    pontuacao = 200;
    vida = 4;
	for(int i = 0; i < 13; i++){
		for(int j = 0; j < 13; j++){
			tabuleiro_do_jogador[i][j] = '-';
		}
	}
}

Jogador::~Jogador() {
}

string Jogador::get_nome(){
    return nome;
}

void Jogador::set_nome(string nome){
    this->nome = nome;
}

int Jogador::get_pontuacao(){
    return pontuacao;
}

void Jogador::set_pontuacao(int pontuacao){
    this->pontuacao = pontuacao;
}

int Jogador::get_vida(){
    return vida;
}

void Jogador::set_vida(int vida){
    this->vida = vida;
}

bool Jogador::minhas_embarcacoes_restantes(){
    if(canoa.empty() && submarino.empty() && portaAvioes.empty()){
        return false;
    }
    else{
        return true;
    }
}

void Jogador::posicionar_embarcacao(){
	string player;
	cin >> player;
    cout << endl;

	string linha;
	ifstream meu_mapa;
	meu_mapa.open("doc/mapa_" + player + ".txt"); //Digitar: player1 ou player2
	if (meu_mapa.is_open()) {
		while (!meu_mapa.eof()) {
			getline (meu_mapa, linha);
			if(linha[0] == '#' || linha[0] == ' ') {
				continue;
			}
			istringstream trecho {linha};
			string coord_x, coord_y, tipo, orientacao;
			int x, y;
			trecho >> coord_x;
			trecho >> coord_y;
			x = stoi(coord_x);
			y = stoi(coord_y);
			trecho >> tipo;
			trecho >> orientacao;

			if(tipo == "canoa"){
				Canoa canoa1;
				canoa1.set_posLinha(x);
				canoa1.set_posColuna(y);
				tabuleiro_do_jogador[canoa1.get_posLinha()][canoa1.get_posColuna()] = 'c';
				canoa.push_back(canoa1);
			}
			else if(tipo == "submarino"){
				Submarino submarino1;
				submarino1.set_posLinha(x);
				submarino1.set_posColuna(y);
				submarino1.set_orientacao(orientacao);

				if(submarino1.get_orientacao() == "esquerda"){
					for(int i = 0; i < 2; i++) {
						tabuleiro_do_jogador[submarino1.get_posLinha()][submarino1.get_posColuna() - i] = 'S';
					}
				}
				else if(submarino1.get_orientacao() == "direita"){
					for(int i = 0; i < 2; i++) {
						tabuleiro_do_jogador[submarino1.get_posLinha()][submarino1.get_posColuna() + i] = 'S';
					}
				}
				else if(submarino1.get_orientacao() == "cima"){
					for(int i = 0; i < 2; i++) {
						tabuleiro_do_jogador[submarino1.get_posLinha() - i][submarino1.get_posColuna()] = 'S';
					}
				}
				else if(submarino1.get_orientacao() == "baixo"){
					for(int i = 0; i < 2; i++) {
						tabuleiro_do_jogador[submarino1.get_posLinha() + i][submarino1.get_posColuna()] = 'S';
					}
				}
				submarino.push_back(submarino1);
			}
			else if(tipo == "portaAvioes"){
				PortaAvioes portaAvioes1;
				portaAvioes1.set_posLinha(x);
				portaAvioes1.set_posColuna(y);
				portaAvioes1.set_orientacao(orientacao);

				if(portaAvioes1.get_orientacao() == "esquerda"){
					for(int i = 0; i < 4; i++) {
						tabuleiro_do_jogador[portaAvioes1.get_posLinha()][portaAvioes1.get_posColuna() - i] = 'p';
					}
				}
				else if(portaAvioes1.get_orientacao() == "direita"){
					for(int i = 0; i < 4; i++) {
						tabuleiro_do_jogador[portaAvioes1.get_posLinha()][portaAvioes1.get_posColuna() + i] = 'p';
					}
				}
				else if(portaAvioes1.get_orientacao() == "cima"){
					for(int i = 0; i < 4; i++) {
						tabuleiro_do_jogador[portaAvioes1.get_posLinha() - i][portaAvioes1.get_posColuna()] = 'p';
					}
				}
				else if(portaAvioes1.get_orientacao() == "baixo"){
					for(int i = 0; i < 4; i++) {
						tabuleiro_do_jogador[portaAvioes1.get_posLinha() + i][portaAvioes1.get_posColuna()] = 'p';
					}
				}
				portaAvioes.push_back(portaAvioes1);
			}
		}
		meu_mapa.close();
	}
	else cout << "Não foi possível abrir o arquivo!" << endl;
}

void Jogador::atacar_inimigo(Jogador *inimigo){
    int linha;
    int coluna;
    char enter;

    Mapa mapa;
    mapa.exibir_mapa();

    cout << endl;
    cout << "ATACAR O INIMIGO!" << endl << "Escolha a linha (0 - 12): ";
    cin >> linha;
    cout << endl;
    cout << "Escolha uma coluna (0 - 12): ";
    cin >> coluna;
    cout << endl;
    
	if(inimigo->tabuleiro_do_jogador[linha][coluna] == '#'){
		while(inimigo->tabuleiro_do_jogador[linha][coluna] == '#'){
			cout << "VOCE JA DESTRUIU O NAVIO INIMIGO NESSA POSICAO. ESCOLHA AS COORDENADAS NOVAMENTE!" << endl;
			cout << "Escolha outra linha (0 - 12): ";
			cin >> linha;
    		cout << endl;
    		cout << "Escolha outra coluna (0 - 12): ";
    		cin >> coluna;
    		cout << endl;
		}
	}
    if(inimigo->tabuleiro_do_jogador[linha][coluna] == 'c'){
        cout << "QUE BELA MIRA! VOCE ACERTOU UMA CANOA!   Pressione qualquer letra para continuar!" << endl << endl;
        inimigo->tabuleiro_do_jogador[linha][coluna] = '#';
        inimigo->canoa.pop_back();
        inimigo->set_pontuacao(inimigo->get_pontuacao() - 10);
        cin >> enter;
    }
    else if(inimigo->tabuleiro_do_jogador[linha][coluna] == 'S'){
        inimigo->tabuleiro_do_jogador[linha][coluna] = 's';
        cout << "VOCE ACERTOU PARTE DE UM SUBMARINO!   Pressione qualquer letra para continuar!" << endl << endl;
        cin >> enter;
    }
    else if(inimigo->tabuleiro_do_jogador[linha][coluna] == 's'){
        inimigo->tabuleiro_do_jogador[linha][coluna] = '#';
        if(inimigo->tabuleiro_do_jogador[linha][coluna - 1] != 'S' && inimigo->tabuleiro_do_jogador[linha][coluna + 1] != 'S' && inimigo->tabuleiro_do_jogador[linha - 1][coluna] != 'S' && inimigo->tabuleiro_do_jogador[linha + 1][coluna] != 'S'){
            if(inimigo->tabuleiro_do_jogador[linha][coluna - 1] != 's' && inimigo->tabuleiro_do_jogador[linha][coluna + 1] != 's' && inimigo->tabuleiro_do_jogador[linha - 1][coluna] != 's' && inimigo->tabuleiro_do_jogador[linha + 1][coluna] != 's'){
                cout << "VOCE DESTRUIU UM SUBMARINO POR COMPLETO!   Pressione qualquer letra para continuar!";
                inimigo->submarino.pop_back();
                inimigo->set_pontuacao(inimigo->get_pontuacao() - 10);
                cin >> enter;
            }
            else{
                cout << "VOCE ACERTOU PARTE DE UM SUBMARINO!   Pressione qualquer letra para continuar!" << endl << endl;
                cin >> enter;
            }
        }
        else{
            cout << "VOCE ACERTOU PARTE DE UM SUBMARINO!   Pressione qualquer letra para continuar!" << endl << endl;
            cin >> enter;
        }
    }
    else if(inimigo->tabuleiro_do_jogador[linha][coluna] == 'p'){
        inimigo->tabuleiro_do_jogador[linha][coluna] = '#';
        for(int j = (coluna - 1); j < (coluna - 3); j--){
            if(j < 0) {
                break;
            }
            else if(inimigo->tabuleiro_do_jogador[linha][j] == 'p'){
                cout << "VOCE ATINGIU UM PORTA-AVIOES!   Pressione qualquer letra para continuar!" << endl << endl;
                inimigo->set_vida(inimigo->get_vida() - 1);
                cin >> enter;
                break;
            }
            else continue;
        }
        for(int j = (coluna + 1); j < (coluna + 3); j++){
            if(j > 12) {
                break;
            }
            else if(inimigo->tabuleiro_do_jogador[linha][j] == 'p'){
                cout << "VOCE ATINGIU UM PORTA-AVIOES!   Pressione qualquer letra para continuar!" << endl << endl;
                inimigo->set_vida(inimigo->get_vida() - 1);
                cin >> enter;
                break;
            }
            else continue;
        }
        for(int i = (linha - 1); i < (linha - 3); i--){
            if(i < 0) {
                break;
            }
            else if(inimigo->tabuleiro_do_jogador[i][coluna] == 'p'){
                cout << "VOCE ATINGIU UM PORTA-AVIOES!   Pressione qualquer letra para continuar!" << endl << endl;
                inimigo->set_vida(inimigo->get_vida() - 1);
                cin >> enter;
                break;
            }
            else continue;
        }
        for(int i = (linha + 1); i < (linha + 3); i++){
            if(i > 12) {
                break;
            }
            else if(inimigo->tabuleiro_do_jogador[i][coluna] == 'p'){
                cout << "VOCE ATINGIU UM PORTA-AVIOES!   Pressione qualquer letra para continuar!" << endl << endl;
                inimigo->set_vida(inimigo->get_vida() - 1);
                cin >> enter;
                break;
            }
        }
        if(inimigo->get_vida() == 0){ 
            cout << "VOCE ATINGIU UM PORTA-AVIOES!   Pressione qualquer letra para continuar!" << endl << endl;
            inimigo->portaAvioes.pop_back();
            inimigo->set_pontuacao(inimigo->get_pontuacao() - 10);
            cin >> enter;
            inimigo->set_vida(4);
        }
    }
    else{
        cout << "E MELHOR VOCE MELHORAR ESSA SUA MIRA NA PROXIMA RODADA, VOCE ACERTOU A AGUA!   Pressione qualquer letra para continuar!" << endl << endl;
        inimigo->set_pontuacao(inimigo->get_pontuacao() + 10);
        cin >> enter;
    }
}