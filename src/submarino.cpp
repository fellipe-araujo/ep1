#include "../inc/submarino.hpp"
#include <iostream>
#include <string>

Submarino::Submarino(){
	//cout << "Construtor da Classe Submarino" << endl;
}

Submarino::Submarino(int posLinha, int posColuna, string orientacao){
	set_posLinha(posLinha);
	set_posColuna(posColuna);
	set_orientacao(orientacao);

	//cout << "Construtor Sobrecarregado da Classe Submarino" << endl;
}

Submarino::~Submarino(){
	//cout << "Destrutor da Classe Submarino" << endl;
}

string Submarino::get_orientacao(){
	return orientacao;
}

void Submarino::set_orientacao(string orientacao){
	this->orientacao = orientacao;
}