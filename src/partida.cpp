#include "../inc/partida.hpp"
#include "../inc/navio.hpp"
#include "../inc/canoa.hpp"
#include "../inc/submarino.hpp"
#include "../inc/portaAvioes.hpp"
#include "../inc/mapa.hpp"
#include "../inc/jogador.hpp"
#include <bits/stdc++.h>

Partida::Partida(){
    cout << "Jogo iniciado!" << endl; 
}

Partida::~Partida(){
    cout << "Partida finalizada!" << endl << endl;
}

bool Partida::get_fim(){
    return fim;
}

void Partida::set_fim(bool fim){
    this->fim = fim;
}

void Partida::Game(){
    string nome_jogador_1;
    string nome_jogador_2;
    int turno = 1;

    cout << "JOGADOR 1, ESCOLHA UM NOME PARA A SUA FROTA: ";
    cin >> nome_jogador_1;
    cout << endl;

    cout << "JOGADOR 2, ESCOLHA UM NOME PARA A SUA FROTA: ";
    cin >> nome_jogador_2;
    cout << endl;

    Jogador *j1 = new Jogador();
    j1->set_nome(nome_jogador_1);

    Jogador *j2 = new Jogador();
    j2->set_nome(nome_jogador_2);

    cout << "Para carregar o mapa do Jogador 1, digite 'player1' e tecle enter: ";
    j1->posicionar_embarcacao();
    cout << "Para carregar o mapa do Jogador 2, digite 'player2' e tecle enter: ";
    j2->posicionar_embarcacao();

    Jogador *atacante = new Jogador();
    Jogador *atacado = new Jogador();
    Jogador *vencedor = new Jogador();
    Jogador *perdedor = new Jogador();

    do{
        if(turno == 1) {
            atacante = j1;
            atacado = j2;
        }
        else{
            atacante = j2;
            atacado = j1;
        }
        system("clear");
        cout << "AGORA E A VEZ DO(A) " << atacante->get_nome() << endl;
        atacante->atacar_inimigo(atacado);
        if(!atacado->minhas_embarcacoes_restantes()){
            vencedor = atacante;
            perdedor = atacado;
            fim = true;
        }
        if(turno == 1){
            turno++;
        }
        else{
            turno--;
        }
    }while(fim == false);
    vencedor->set_pontuacao(vencedor->get_pontuacao() + 50);

    system("clear");
    cout << "FIM DE JOGO!" << endl;
    cout << "O VENCEDOR FOI " << vencedor->get_nome() << " COM UM TOTAL DE " << vencedor->get_pontuacao() << " PONTOS!" << endl;
    cout << "O PERDEDOR FOI " << perdedor->get_nome() << " COM UM TOTAL DE " << perdedor->get_pontuacao() << " PONTOS!" << endl;
}