#include "../inc/portaAvioes.hpp"
#include <iostream>
#include <string>

PortaAvioes::PortaAvioes(){
	//cout << "Construtor da Classe PortaAvioes" << endl;
}

PortaAvioes::PortaAvioes(int posLinha, int posColuna, string orientacao){
	set_posLinha(posLinha);
	set_posColuna(posColuna);
	set_orientacao(orientacao);

	//cout << "Construtor Sobrecarregado da Classe PortaAvioes" << endl;
}

PortaAvioes::~PortaAvioes(){
	//cout << "Destrutor da Classe PortaAvioes" << endl;
}

string PortaAvioes::get_orientacao(){
	return orientacao;
}

void PortaAvioes::set_orientacao(string orientacao){
	this->orientacao = orientacao;
}

bool PortaAvioes::abater_missel(){
	srand(time(NULL));
	int missel_nao_abatido = 0;

	if(rand() % 2 == missel_nao_abatido){
		return false;
	}
	else{
		return true;
	}
}