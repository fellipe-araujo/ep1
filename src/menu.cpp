#include "../inc/menu.hpp"
#include "../inc/partida.hpp"
#include <iostream>

template <typename T1>
T1 getInput(){
    while(true){
        T1 valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida. Insira novamente: ";            
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }    
}

Menu::Menu() {
    cout << "ØØØØØØØØ   ØØØØØØØØ   ØØØØØØØØØ   ØØØØØØØØ   Ø          Ø      Ø   ØØØØØØØØ       ØØ        Ø   ØØØØØØØØ   Ø      Ø   ØØØØØØØØ   Ø" << endl;
    cout << "Ø      Ø   Ø      Ø       Ø       Ø      Ø   Ø          Ø      Ø   Ø      Ø       Ø Ø       Ø   Ø      Ø   Ø      Ø   Ø      Ø   Ø" << endl;
    cout << "Ø      Ø   Ø      Ø       Ø       Ø      Ø   Ø          Ø      Ø   Ø      Ø       Ø  Ø      Ø   Ø      Ø   Ø      Ø   Ø      Ø   Ø" << endl;
    cout << "Ø      Ø   Ø      Ø       Ø       Ø      Ø   Ø          Ø      Ø   Ø      Ø       Ø   Ø     Ø   Ø      Ø   Ø      Ø   Ø      Ø   Ø" << endl;
    cout << "ØØØØØØØ    ØØØØØØØØ       Ø       ØØØØØØØØ   Ø          ØØØØØØØØ   ØØØØØØØØ       Ø    Ø    Ø   ØØØØØØØØ   Ø      Ø   ØØØØØØØØ   Ø" << endl;
    cout << "Ø      Ø   Ø      Ø       Ø       Ø      Ø   Ø          Ø      Ø   Ø      Ø       Ø     Ø   Ø   Ø      Ø   Ø      Ø   Ø      Ø   Ø" << endl;
    cout << "Ø      Ø   Ø      Ø       Ø       Ø      Ø   Ø          Ø      Ø   Ø      Ø       Ø      Ø  Ø   Ø      Ø   Ø      Ø   Ø      Ø   Ø" << endl;
    cout << "Ø      Ø   Ø      Ø       Ø       Ø      Ø   Ø          Ø      Ø   Ø      Ø       Ø       Ø Ø   Ø      Ø    Ø    Ø    Ø      Ø   Ø" << endl;
    cout << "Ø      Ø   Ø      Ø       Ø       Ø      Ø   Ø          Ø      Ø   Ø      Ø       Ø        ØØ   Ø      Ø     Ø  Ø     Ø      Ø   Ø" << endl;
    cout << "ØØØØØØØØ   Ø      Ø       Ø       Ø      Ø   ØØØØØØØØ   Ø      Ø   Ø      Ø       Ø         Ø   Ø      Ø      ØØ      Ø      Ø   ØØØØØØØØ" << endl;
    cout << endl << endl;

    while(true) {
        cout << "BEM VINDO!" << endl << endl;
        cout << "1. Jogar" << endl;
        cout << "2. Regras" << endl;
        cout << "3. Sair" << endl << endl;
        cout << "Escolha uma das opcoes acima (digitar somente o numero): ";

        resposta = getInput<int>();

        switch(resposta){
            case 1: {
                system("clear");
                Partida p;
                p.Game();
                break;
            }case 2: {
                char enter;
                system("clear");
                cout << "REGRAS:" << endl;
                cout << "1. Canoa ocupa uma casa, Submarino ocupa duas casas, mas cada casa precisa ser atingida duas vezes para ser abatida" << endl;
                cout << " e o Porta-Avioes ocupa 4 casas (ele tem o poder de abater misseis randomicamente);" << endl << endl;
                cout << "2. O mapa do jogo e utilizado somente como referencia para se escolher uma casa para jogar uma bomba, nao mostrando," << endl;
                cout << " assim, onde ja foi lancada uma bomba e onde um navio inimigo foi abatido. Dessa forma, o jogo se torna mais interessante," << endl;
                cout << " fazendo com que o jogador memorize aonde ele ja atacou;" << endl << endl;
                cout << "3. A pontuacao do jogo e feita da seguinte forma: cada jogador comeca com 200 pontos, se o jogador abate um navio inimigo," << endl;
                cout << " o inimigo perde 10 pontos. Mas se o jogador acerta a agua, o inimigo ganha 10 pontos. No final, o vencedor ganha 50 pontos" << endl;
                cout << " pela vitoria." << endl << endl;
                cout << "Aperte uma tecla e depois tecle enter. . .";
                cin >> enter;
                cout << endl;
                break;
            }case 3:
                exit(0);
                break;
            default:
                break;
        }
    }
}

Menu::~Menu() {
}

int Menu::get_resposta(){
    return resposta;
}