#include "../inc/canoa.hpp"
#include <iostream>

Canoa::Canoa(){
	//cout << "Construtor da Classe Canoa" << endl;
}

Canoa::Canoa(int posLinha, int posColuna){
	set_posLinha(posLinha);
	set_posColuna(posColuna);

	//cout << "Construtor Sobrecarregado da Classe Canoa" << endl;
}

Canoa::~Canoa(){
	//cout << "Destrutor da Classe Canoa" << endl;
}