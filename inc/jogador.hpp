#ifndef JOGADOR_HPP
#define JOGADOR_HPP

#include <bits/stdc++.h>
#include "../inc/mapa.hpp"

using namespace std;

class Jogador {
    private:
        string nome;
        int pontuacao;
        int vida;
    public:
        vector<Canoa> canoa;
        vector<Submarino> submarino;
        vector<PortaAvioes> portaAvioes;

        char tabuleiro_do_jogador[13][13];

        Jogador();
        ~Jogador();

        string get_nome();
        void set_nome(string nome);

        int get_pontuacao();
        void set_pontuacao(int pontuacao);

        int get_vida();
        void set_vida(int vida);

        void posicionar_embarcacao();
        void atacar_inimigo(Jogador *inimigo);
        bool minhas_embarcacoes_restantes();
};

#endif