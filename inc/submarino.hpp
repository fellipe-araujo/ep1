#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "../inc/navio.hpp"
#include <string>

using namespace std;

class Submarino : public Navio {
	private:
		string orientacao;
	public:
		Submarino();
		Submarino(int posLinha, int posColuna, string orientacao);
		~Submarino();

		string get_orientacao();
		void set_orientacao(string orientacao);
};

#endif