#ifndef MENU_HPP
#define MENU_HPP

using namespace std;

class Menu {
    private:
        int resposta;
    public:
        Menu();
        ~Menu();

        int get_resposta();
};

#endif