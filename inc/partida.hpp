#ifndef PARTIDA_HPP
#define PARTIDA_HPP

using namespace std;

class Partida{
    private:
        bool fim;
    public:
        Partida();
        ~Partida();

        bool get_fim();
        void set_fim(bool fim);
        void Game();
};

#endif